# Cython example codes



## Description
This repository contains example codes to call from Python functions written in C using [Cython](http://cython.org/).
These examples were discussed at YAM 2017 at IUCAA. The presentation slides are present in the Cython tutorial YAM2017.pdf file


## Installing dependencies

This code has the following package dependencies:
 * cython
### via pip

```sudo pip install cython```

### via [Anaconda](https://www.continuum.io/what-is-anaconda) distribution
If you had installed Anaconda, you can install the above dependencies using the `conda` command.
```
conda install cython
```

## For [PyCharm](https://www.jetbrains.com/pycharm/) IDE users

I have uploaded the Cython settings for PyCharm IDE in the file ```PyCharm_Cython_Syntax_Settings.jar``` which will enable the IDE to highlight Cython code. With the settings imported into PyCharm IDE, the IDE will recognise .pyx and .pyd files, will highlight Cython syntax and also show typing help.
To import the settings, goto ```File->ImportSettings->```. Point to the ```PyCharm_Cython_Syntax_Settings.jar``` location and the feature will be enabled.

## Contribution guidelines

I am uploading the software **AS IS**. Currently, there is no documentation available. I have not adhered to PEP8 guidelines -the standard for open-source Python projects.
You are free to contribute in whichever way you feel like such as, adding a feature, documentation, making the code compatible to PEP8 guidelines or anything else you can think of that will make the software useful to the community.

## Disclaimer

This software has been uploaded **AS IS**. Though I had used it on Linux and Windows, there is no guarantee that this software is bug-free. Use with caution.

## Credits

Ideas taken from [Cython docs](http://cython.org/), [zed on github](https://gist.github.com/zed/4e72cc3ac15408df452e).


## License
![Alt text](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)
This work is licensed under [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).