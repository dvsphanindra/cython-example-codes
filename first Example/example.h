#ifndef __EXAMPLE_H__
	# define __EXAMPLE_H__
	
	float add(float a, float b);
	
	int subtract(int a, int b);
	
	float divide(float a, float b);

	void assign_values1D(double matrix[], unsigned int size);
	
	void square_values2D(double **matrix, unsigned int row_size, unsigned int column_size);

	void addVectors(const double vector1[], const double vector2[], double resultVector[], int size);

#endif
