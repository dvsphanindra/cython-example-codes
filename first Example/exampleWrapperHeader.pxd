""" 
	This is a Cython header file which is same as the C header file with some minor changes.
	Use this .pxd file to make wrapper functions to the API in C header file.
	Only those functions which are to be used in Python can be wrapped.
"""

cdef extern from "example.h":
	float add(float a, float b);
	
	int subtract(int a, int b);

	float divide(float a, float b);

	void assign_values1D(double matrix[], unsigned int size);

	void square_values2D(double **matrix, unsigned int row_size, unsigned int column_size);

	void addVectors(const double vector1[], const double vector2[], double resultVector[], int size)
	
