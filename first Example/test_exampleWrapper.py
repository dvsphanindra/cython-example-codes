# -*- coding: utf-8 -*-
# Author: D.V.S. Phanindra
# Created on 07 Jun, 2017 at 11:11 AM
#
from libFirstExample import add_Wrapper, add_Wrapper2, subtract_Wrapper, addVectorsWrapper, assignValues1D, squareValues2D, matMulStaticTyped, matMulStaticTypedBoundsUnchecked
import numpy as np
import timeit

print add_Wrapper(2 ,3)
print add_Wrapper2(200 ,300)

list1 = [20, 30, 40]
list2 = [30, 40, 50]
print subtract_Wrapper(list1, list2)


# Create an empty matrix
a = np.empty(5, dtype=np.float)
assignValues1D(a)
print a

# Create a random matrix
row = 4
column = 5
b = np.arange(0,row*column, dtype=np.float)
b = np.reshape(b, (row, column))
print "2D Matrix before:"
print b
squareValues2D(b)
print "2D Matrix after:"
print b

# Addition of vectors
vectorSize = 10
v1 = np.arange( vectorSize, dtype=np.float64 )
v2 = np.arange( vectorSize, dtype=np.float64 )
r = np.empty( vectorSize, dtype=np.float64 )

# The size can be passed or it can be determined in the .pyx
addVectorsWrapper(v1, v2, r, vectorSize)
print "Result:\n", r


################################################################################

def calculateExecutionTime(function):
	def functionWrapper(mat1,b):
		start = timeit.default_timer()
		function(mat1, b)
		end = timeit.default_timer()
		return "\nTime taken to run the function {0}: {1:5.3f} msec".format(function.__name__, (end - start)/1E-3)
	
	return functionWrapper

#################################################################################

@calculateExecutionTime
def matrixMultiplication(mat1, mat2):
	result = np.zeros((np.shape(mat1)[0], np.shape(mat2)[1]), dtype=np.float64)
	for i in range(np.shape(mat1)[0]):
		for j in range(np.shape(mat2)[1]):
			
			for k in range(np.shape(mat1)[1]):
				result[i][j] += mat1[i][k] * mat2[k][j]
				
	# print "Matrix multiplication result ="
	# print result




# Create mat1 random matrix
row =10
column = 10
mat1 = np.arange(0, row * column, dtype=np.float)
mat1 = np.reshape(mat1, (row, column))
# print "Matrix mat1 = "
# print mat1

mat2 = np.arange(0, row * column, dtype=np.float)
mat2 = np.reshape(mat2, (column, row))
# print "Matrix mat2 = "
# print mat2

print matrixMultiplication(mat1, mat2)

start = timeit.default_timer()
matMulStaticTyped(mat1, mat2)
end = timeit.default_timer()
print "Cython with static typed matrix multiplication timing = {0:5.3f} msec".format((end-start)/1E-3)


start = timeit.default_timer()
matMulStaticTypedBoundsUnchecked(mat1, mat2)
end = timeit.default_timer()
print "Cython with static typed bounds unchecked matrix multiplication timing = {0:5.3f} msec".format((end-start)/1E-3)

start = timeit.default_timer()
np.dot(mat1, mat2)
end = timeit.default_timer()
print "Cython with numpy matrix multiplication timing = {0:5.3f} msec".format((end-start)/1E-3)
