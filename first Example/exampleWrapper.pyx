"""
This is the wrapper for the C-library. The name of the *.pxd file will be used to import
"""

from exampleWrapperHeader cimport *
import numpy as np
cimport numpy as np
cimport cython
from libc.stdlib cimport malloc, free
import ctypes


# 'Python-like' function, but types are declared
# Cython is capable of interpreting correctly even though the types are declared
def add_Wrapper(double x, double y):
	return add(x,y)

# cpdef example. This is to facilitate calling from python but accepting typed inputs
# Fails to execute when there is type-mismatch
# Can be declared as def also as Cython is capable of interpreting correctly
# cpdef used generally for recursion
cpdef add_Wrapper2(double x, double y):
	return add(x,y)

# Ordinary python-like function. But inside, it is calling a c function subtract()
def subtract_Wrapper(list1, list2):
	result = []
	for i in range(len(list1)):
		result.append(subtract(list1[i],list2[i]))
	return result


# These are to ensure faster calculations in Cython
@cython.boundscheck(False)
@cython.wraparound(False)
def matMulStaticTypedBoundsUnchecked(np.ndarray[np.double_t,ndim=2,mode='c'] mat1, np.ndarray[np.double_t,ndim=2,mode='c']mat2):
	result = np.zeros((np.shape(mat1)[0], np.shape(mat2)[1]), dtype=np.float64)

	cdef int i, j, k

	for i in range(np.shape(mat1)[0]):
		for j in range(np.shape(mat2)[1]):

			for k in range(np.shape(mat1)[1]):
				result[i][j] += mat1[i][k] * mat2[k][j]

def matMulStaticTyped(mat1, mat2):
	result = np.zeros((np.shape(mat1)[0], np.shape(mat2)[1]), dtype=np.float64)

	cdef int i, j, k

	for i in range(np.shape(mat1)[0]):
		for j in range(np.shape(mat2)[1]):

			for k in range(np.shape(mat1)[1]):
				result[i][j] += mat1[i][k] * mat2[k][j]

def addVectorsWrapper(np.ndarray[np.double_t,ndim=1] vector1,
			np.ndarray[np.double_t,ndim=1] vector2,
			np.ndarray[np.double_t,ndim=1] resultVector, size=0):
	# Calculate the size if it not given
	if size == 0:
		size = np.shape(vector1)[0]

	addVectors(<double*> &vector1[0], <double*> &vector2[0], <double*> &resultVector[0], size)

def assignValues1D(np.ndarray[np.double_t,ndim=1] vector1):
# def assignValues1D(double [::1] vector1):
	size = np.shape(vector1)[0]
	assign_values1D(&vector1[0], size)

# This function is accepting as input, a typed parameter of type: np.ndarray[np.double_t,ndim=2,mode='c']
# Fails to execute when there is type-mismatch
def squareValues2D(np.ndarray[np.double_t,ndim=2,mode='c'] A):
# def assignValues2D(double[:,::1] A):
	cdef int row_size, column_size
	row_size,column_size = np.shape(A)

	# Assign correct memory order
	cdef np.ndarray[np.double_t, ndim=2, mode="c"] temp_mat = np.ascontiguousarray(A, dtype = ctypes.c_double)

	# Create an intermediate 2D pointer array
	cdef double ** pointer2A = <double **>malloc(column_size * sizeof(double*))

	if not pointer2A:
		raise MemoryError
	try:
		for i in range(row_size):
			pointer2A[i] = &temp_mat[i, 0]

		# Pass this double** to the C function
		square_values2D(<double **> &pointer2A[0], row_size, column_size)
		return np.array(A)
	finally:
		free(pointer2A)
