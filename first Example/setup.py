# -*- coding: utf-8 -*-
# Author: D.V.S. Phanindra
# Created on 06 Jun, 2017 at 3:31 PM
# 

"""
Compile using: python setup.py build_ext --inplace
"""

from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import numpy as np
import os
# Get the directory in which the setup.py file is present
fileDirName = os.path.dirname(__file__)
setup(
	cmdclass = {'build_ext': build_ext},
	ext_modules=    [
		Extension(  "libFirstExample",		            # Name with which the library should be imported into python
					sources=  [	"exampleWrapper.pyx",
								"example.c"	# Add the c source file to ensure addition of the lib*.so files to compilation(for safety)
								],
					#libraries=["example"],        # refers to "libexample.so"
		            # define_macros = [('NPY_NO_DEPRECATED_API', 'NPY_1_7_API_VERSION')], # Use when needed
		            include_dirs=[np.get_include()], # Use if numpy headers are not included automatically
		            library_dirs=[fileDirName],  # All the libraries are present in the same directory
				)
		]
)           
