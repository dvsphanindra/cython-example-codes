/*
 * Compile using:
 * 
 * gcc -fpic -shared test_so.c -o libtest_so.so
 * 
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "example.h"

float add(float a, float b){
	return a + b;
}

int subtract(int a, int b){
	return a - b;
}

float divide(float a, float b){
	return a / b;
}

void addVectors(const double vector1[], const double vector2[], double resultVector[], int size){
    int i;

    for(i = 0;  i < size;  i++)
    	resultVector[i] = vector1[i] + vector2[i];
}

void assign_values1D(double matrix[], unsigned int size){
	unsigned int i;
	
	for(i=0; i<size; i++){
		matrix[i] = i * i;
	}
}

void square_values2D(double **matrix, unsigned int row_size, unsigned int column_size){
	unsigned int i, j;
	
	for(i=0; i<row_size; i++){
		for(j=0; j<column_size; j++){
			matrix[i][j] = matrix[i][j] * matrix[i][j];
		}
	}
}
