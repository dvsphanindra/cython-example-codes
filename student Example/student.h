#ifndef __STUDENT_H__
#define __STUDENT_H__

	typedef struct studentStruct {
		unsigned int ID; // Private variable. A unique ID is assigned automatically when the student is created
		char* name;
		unsigned int age;
		unsigned int number;
	} student;

	typedef struct studentStruct * student_ptr;

	// Public functions
	student_ptr student_Create(const char* name,  unsigned int age);
	void student_RemoveRecord(student_ptr self);

	const char* student_GetName(const student_ptr self);
	unsigned int student_GetID(const student_ptr self);
	unsigned int student_GetClass(const student_ptr self);
	int student_GetAge(const student_ptr self);
	void student_birthday2day(student_ptr self);
	unsigned int promoteClass(student_ptr self);

	// Non class methods: Wont take the student_ptr as input parameter
	// Initialize the records to start from a base ID
	unsigned int initializeStudentRecords(int startID);

# endif /* __STUDENT_H__ */
