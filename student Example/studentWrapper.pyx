"""
This is the wrapper for the C-library.
"""
from studentHeader cimport *
import ctypes

cdef class student:
	cdef student_ptr _thisptr # _ to indicate that this is a private variable

	def __cinit__(self, char* name, unsigned int age):
		# __init__() can also be used as malloc is in C
		self._thisptr = student_Create(name, age)
		if self._thisptr is NULL:
			raise MemoryError
			
	def __dealloc__(self):
		if self._thisptr is not NULL:
			student_RemoveRecord(self._thisptr)
	
	property name:
		def __get__(self):
			return student_GetName(self._thisptr)
			
	property ID:
		def __get__(self):
			return student_GetID(self._thisptr)
			
	property standard:
		def __get__(self):
			return student_GetClass(self._thisptr)

	@staticmethod
	def initStudentRecords(number):
		return initializeStudentRecords(number)
	
	def getAge(self):
		return student_GetAge(self._thisptr)
		
	def bDay_today(self):
		return student_birthday2day(self._thisptr)
		
	def promote_Class(self):
		return promoteClass(self._thisptr)
