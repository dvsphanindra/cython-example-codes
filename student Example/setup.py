"""
Compile using: python setup.py build_ext --inplace
"""

import os
from distutils.extension import Extension
from distutils.core import setup
from Cython.Distutils import build_ext

# Get the directory in which the setup.py file is present
fileDirName = os.path.dirname(__file__)

setup(
	cmdclass = {'build_ext': build_ext},
	ext_modules=	[
		Extension(  "libstudent",				 # Name with which the library should be imported into python
					sources=  ["studentWrapper.pyx",
								"student_c.c"		# Add the c source file to ensure addition of the lib*.so files to compilation (for safety)
								],
					library_dirs=[fileDirName], # All the libraries are present in the same directory
		)
		]
)
