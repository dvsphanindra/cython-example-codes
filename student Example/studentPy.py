"""
	Import using the name given as first parameter to Extension() in setup.py file
"""
from libstudent import student

student.initStudentRecords(100)

student1 = student('DVS', 10)
student2 = student('Phanindra', 11)

# Not possible as we are calling __cinit__()
# student3 = student('Name', (30,31))

print "Student 1 details..."
print student1.name
print student1.ID
print student1.getAge()
student1.bDay_today()
print student1.getAge()

print "Student 2 details..."
print student2.name
print student2.ID
print student2.getAge()
student2.bDay_today()
print student2.getAge()

print "Student 2 promotion..."
while student2.promote_Class():
	print student2.standard
