#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>

#include "student.h"

// Private functions: Not accessible from outside
#define STUDENT_CHECK_INSTANCE(x) assert((student_ptr) x != (student_ptr)NULL)

static void student_init(student_ptr self, const char* name, unsigned int age);
static unsigned int UID =0;
static unsigned int MAXCLASS = 10;

student_ptr student_Create(const char* name, unsigned int age){
	student_ptr self = (student_ptr) malloc(sizeof(student));
	student_init(self, name, age);
	return self;
}

void student_RemoveRecord(student_ptr self){
	STUDENT_CHECK_INSTANCE(self);
	if (NULL != self->name) free(self->name);
}

const char* student_GetName(const student_ptr self){
	STUDENT_CHECK_INSTANCE(self);
	return self->name;
}

unsigned int student_GetID(const student_ptr self){
	STUDENT_CHECK_INSTANCE(self);
	return self->ID;
}

int student_GetAge(const student_ptr self){
	STUDENT_CHECK_INSTANCE(self);
	return self->age;
}

unsigned int student_GetClass(const student_ptr self){
	STUDENT_CHECK_INSTANCE(self);
	return self->number;
}

void student_birthday2day(student_ptr self){
	STUDENT_CHECK_INSTANCE(self);
	++self->age;
}

unsigned int promoteClass(student_ptr self){
	STUDENT_CHECK_INSTANCE(self);
	// Promote the student until highest class
	if (self->number >= MAXCLASS)
		return 0;
	else{
		++self->number;
		return 1;
	}
}

// private functions: Not accessible from outside
static void student_init(student_ptr self, const char* name, unsigned int age){
	STUDENT_CHECK_INSTANCE(self);
	self->name = malloc(sizeof(char) * (strlen(name) + 1));
	assert(NULL != self->name);
	strcpy(self->name, name);
	self->age = age;
	self->ID = UID++;
	self->number = 0;
}

// Non object methods: Dont take the student_ptr as input
// Initialize the records to start from a base ID
unsigned int initializeStudentRecords(int startID){
	if (UID == 0){
		if (startID > 0){
			UID = startID;
			return 0;
		}
		else
			return -1;
	}
	else
		return 1;
}

