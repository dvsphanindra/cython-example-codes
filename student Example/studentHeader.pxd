"""
	This is a Cython definition file which is same as the C header file with some minor changes.
	Use this .pyd file to make wrapper functions to the API in C header file.
	Only those functions which are to be used in Python can be wrapped.
	The name of this *.pyd file (studentWrapper) will be used to import the functions
	into the .pyx file
"""
cdef extern from "student.h":
	# Assign only those elements that are necessary
	# pass if all the elements need to be assigned
	cdef struct studentStruct:
		pass
		
	ctypedef studentStruct* student_ptr
	
	student_ptr student_Create(char* name, unsigned int age)
	void student_RemoveRecord(student_ptr self)
	
	char* student_GetName(student_ptr self)
	unsigned int student_GetID(student_ptr self)
	unsigned int promoteClass(student_ptr self)
	unsigned int student_GetClass(student_ptr self)
	
	int student_GetAge(student_ptr self)
	void student_birthday2day(student_ptr self)

	# Non class methods: Wont take the student_ptr (self) as input parameter
	# Initialize the records to start from a base ID
	unsigned int initializeStudentRecords(int startID);